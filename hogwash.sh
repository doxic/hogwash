#!/bin/bash

sudo yum install -y gcc curl vim
curl http://hogwash.sourceforge.net/devel-0.5-latest.tgz | tar xvz
cd distro/devel-0.5/devel-0.5
./configure && make
sudo cp hogwash /sbin
sudo mkdir -p /var/log/hogwash /etc/hogwash
sudo cp rules/*.rules /etc/hogwash
sudo cp *.config /etc/hogwash

# Start hogwash in background
sudo hogwash -c /vagrant/hogwash.config -r /vagrant/hogwash.rules &
disown
