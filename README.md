# Hogwash intrusion detection system
Setting up a Hogwash lab environment with Bait&Switch Honeypot.

## Requirments
- Virtualbox: https://www.virtualbox.org/wiki/Downloads
- Vagrant: https://www.vagrantup.com/downloads.html

## Usage
Start and provision the vagrant environment
```shell
vagrant up
```
Output status of the vagrant machines
```shell
vagrant status
```
Connect to machine via SSH
```shell
vagrant ssh hogwash
vagrant ssh production
vagrant ssh honeypot
```

## Rulesets
Hogwas is setup with two very basic rules
1) action `log` for type 8 icmp echo requests
2) action `route` (to honeypot) for an attemped Chunked Encoding Attack

## Access
Productive access over broser to `http://192.168.1.10`

![Production access with browser](assets/production-access-browser.png)

Chunked encoded request to `http://192.168.1.10:80`
```shell
# curl -H "Transfer-Encoding: chunked" http://192.168.1.10   
honeypot
```


## Environment
![Hogwash Diagram](assets/hogwash-diagram.png)

## Links

- Hogwash, ein Gateway-IDS auf der Basis von Snort: https://www.linux-magazin.de/ausgaben/2003/08/netzwerk-schrubber/2/
- Hogwash Documentation: http://hogwash.sourceforge.net/docs/overview.html
- Snort SSL README: https://www.snort.org/faq/readme-ssl
- How to emulate a network using VirtualBox: https://www.brianlinkletter.com/how-to-use-virtualbox-to-emulate-a-network/
- Setting Up a Honeypot Using a Bait and Switch Router: https://www.sans.org/reading-room/whitepapers/casestudies/setting-honeypot-bait-switch-router-1465