# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.ssh.insert_key = false
  config.vm.box = "centos/7"

  config.vm.define "hogwash" do |hogwash|
    hogwash.vm.hostname = "hogwash"

    hogwash.vm.network :private_network, virtualbox__intnet: "green"
    hogwash.vm.network :private_network, virtualbox__intnet: "red"
    hogwash.vm.network :private_network, ip: "192.168.1.5"

    hogwash.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", 512 ]
      vb.customize ["modifyvm", :id, "--cpus", 1 ]
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      # enable promiscous mode on <1-N>, nicpromisc1 is vagrant NAT interface
      vb.customize ["modifyvm", :id, "--nicpromisc2", "allow-all"]
      vb.customize ["modifyvm", :id, "--nicpromisc3", "allow-all"]
      vb.customize ["modifyvm", :id, "--nicpromisc4", "allow-all"]
      vb.customize ["modifyvm", :id, "--ioapic", "on"]
      vb.linked_clone = true
    end  # config.vm.provider
    
    hogwash.vm.provision "shell", inline: <<-SHELL
      # enable promiscous mode
      ip link set eth1 promisc on
      echo "PROMISC=yes" >> /etc/sysconfig/network-scripts/ifcfg-eth1
      ip link set eth2 promisc on
      echo "PROMISC=yes" >> /etc/sysconfig/network-scripts/ifcfg-eth2
      ip link set eth3 promisc on
      echo "PROMISC=yes" >> /etc/sysconfig/network-scripts/ifcfg-eth3
      # remove external host IP
      sudo ip addr del 192.168.1.5/24 dev eth3
      sudo sed -i '/NETMASK/d' /etc/sysconfig/network-scripts/ifcfg-eth3
      sudo sed -i '/IPADDR/d' /etc/sysconfig/network-scripts/ifcfg-eth3
      # disable ip forwarding
      sudo sysctl -w net.ipv4.ip_forward=0
      echo 0 | sudo tee /proc/sys/net/ipv4/ip_forward
SHELL
    hogwash.vm.provision "shell", path: "hogwash.sh"
  end  # config.vm

  config.vm.define "production" do |production|
    production.vm.hostname = "production"

    production.vm.network :private_network, ip: "192.168.1.10", virtualbox__intnet: "green"

    production.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", 512 ]
      vb.customize ["modifyvm", :id, "--cpus", 1 ]
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      # enable promiscous mode on <1-N>, nicpromisc1 is vagrant NAT interface
      vb.customize ["modifyvm", :id, "--nicpromisc2", "allow-all"]
      vb.customize ["modifyvm", :id, "--ioapic", "on"]
      vb.linked_clone = true
    end  # config.vm.provider

    production.vm.provision "shell", path: "production.sh"
    production.vm.provision "shell", inline: <<-SHELL
      # enable promiscous mode
      ip link set eth1 promisc on
      echo "PROMISC=yes" >> /etc/sysconfig/network-scripts/ifcfg-eth1
SHELL
  end  # config.vm

  config.vm.define "honeypot" do |honeypot|
    honeypot.vm.hostname = "honeypot"

    honeypot.vm.network :private_network, ip: "192.168.1.10", virtualbox__intnet: "red"

    honeypot.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", 512 ]
      vb.customize ["modifyvm", :id, "--cpus", 1 ]
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      # enable promiscous mode on <1-N>, nicpromisc1 is vagrant NAT interface
      vb.customize ["modifyvm", :id, "--nicpromisc2", "allow-all"]
      vb.customize ["modifyvm", :id, "--ioapic", "on"]
      vb.linked_clone = true
    end  # config.vm.provider

    honeypot.vm.provision "shell", path: "honeypot.sh"
    honeypot.vm.provision "shell", inline: <<-SHELL
      # enable promiscous mode
      ip link set eth1 promisc on
      echo "PROMISC=yes" >> /etc/sysconfig/network-scripts/ifcfg-eth1
SHELL
  end  # config.vm
end  # Vagrant.configure
