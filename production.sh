#!/bin/bash

sudo yum install -y vim epel-release
sudo yum install -y nginx
sudo systemctl start nginx

echo $HOSTNAME | sudo tee /usr/share/nginx/html/index.html